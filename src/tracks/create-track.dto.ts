export class CreateTrackDto {
  trackname: string;
  album: string;
  duration: string;
}
