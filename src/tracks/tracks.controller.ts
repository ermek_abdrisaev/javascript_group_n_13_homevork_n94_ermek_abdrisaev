import { Body, Controller, Delete, Get, Param, Post, Query } from "@nestjs/common";
import { InjectModel } from '@nestjs/mongoose';
import { Track, TrackDocument } from '../schemas/track.schema';
import { Model } from 'mongoose';
import { CreateTrackDto } from './create-track.dto';

@Controller('tracks')
export class TracksController {
  constructor(
    @InjectModel(Track.name)
    private trackModel: Model<TrackDocument>,
  ) {}
  @Get()
  getAll(@Query() album: string) {
    let query = {};
    const arrQueryId = Object.values(album);
    if (arrQueryId.length > 0) {
      query = { album: { _id: arrQueryId[0] } };
    }
    return this.trackModel.find(query).populate('album', 'title');
  }
  @Post()
  create(@Body() trackDto: CreateTrackDto) {
    const track = new this.trackModel({
      trackname: trackDto.trackname,
      album: trackDto.album,
      duration: trackDto.duration,
    });
    return track.save();
  }
  @Get(':id')
  getOne(@Param('id') id: string) {
    return this.trackModel.find({ _id: id });
  }
  @Delete(':id')
  eraseOne(@Param('id') id: string) {
    return this.trackModel.deleteOne({ _id: id });
  }
}
