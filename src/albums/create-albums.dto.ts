export class CreateAlbumsDto {
  artist: string;
  title: string;
  release: number;
}
