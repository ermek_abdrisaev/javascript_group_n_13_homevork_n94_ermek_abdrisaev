import { Body, Controller, Delete, Get, Param, Post, Query } from "@nestjs/common";
import { Album, AlbumDocument } from '../schemas/album.schema';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CreateAlbumsDto } from './create-albums.dto';

@Controller('albums')
export class AlbumsController {
  constructor(
    @InjectModel(Album.name) private albumModel: Model<AlbumDocument>,
  ) {}
  @Get()
  getAll(@Query() artist: string) {
    let query = {};
    const arrQueryId = Object.values(artist);
    if (arrQueryId.length > 0) {
      query = { artist: { _id: arrQueryId[0] } };
    }
    return this.albumModel.find(query).populate('artist', 'name');
  }

  @Get(':id')
  getOne(@Param('id') id: string) {
    return this.albumModel.find({ _id: id });
  }

  @Post()
  create(@Body() albumDto: CreateAlbumsDto) {
    const album = new this.albumModel({
      artist: albumDto.artist,
      title: albumDto.title,
      release: albumDto.release,
    });
    return album.save();
  }

  @Delete(':id')
  eraseOne(@Param('id') id: string) {
    return this.albumModel.deleteOne({ _id: id });
  }
}
